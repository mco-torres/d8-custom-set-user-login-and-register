<?php
/**
 * @file
 * Contains \Drupal\mt_login\Form\FiltersForm by Marco Torres marquillo01@gmail.com.
 */

namespace Drupal\mt_login\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\mt_login\MTLoginProvider;

/**
 * Filters form.
 */
class FiltersForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mt_login_filters_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $filters = NULL) {

    $option_types = [
      'login' => t('Login'),
      'register' => t('Register')
    ];

    $form = [
      '#attributes' => [
        'onSubmit' => 'return sendForm()',
        'class' => ['container-inline']
      ],
      'login_filter_id' => [
        '#type' => 'textfield',
        '#title' => t('ID'),
        '#size' => 20,
        '#maxlength' => 6,
        '#default_value' => !empty($filters['id']) ? $filters['id'] : '',
        '#required' => FALSE
      ],
      'login_filter_uid' => [
        '#type' => 'textfield',
        '#title' => t('UID'),
        '#size' => 20,
        '#maxlength' => 11,
        '#default_value' => !empty($filters['uid']) ? $filters['uid'] : '',
        '#required' => FALSE
      ],
      'login_filter_ip' => [
        '#type' => 'textfield',
        '#title' => t('IP'),
        '#size' => 40,
        '#maxlength' => 20,
        '#default_value' => !empty($filters['ip']) ? $filters['ip'] : '',
        '#required' => FALSE
      ],
      'login_filter_type' => [
        '#type' => 'select',
        '#title' => t('Type'),
        '#empty_option' => t('All'),
        '#options' => $option_types,
        '#default_value' => isset($filters['type']) ? $filters['type'] : '',
        '#required' => FALSE
      ]
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => t('Filter'),
    ];

    if (!empty($filters)) {
      $form['reset'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#submit' => array('::resetForm'),
      );
    }

    $form['#attached']['library'][] = 'mt_login/mt-login';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $filters = [
      'id' => $values['login_filter_id'],
      'uid' => $values['login_filter_uid'],
      'ip' => $values['login_filter_ip'],
      'type' => $values['login_filter_type'],
    ];
    $form_state->setRedirect('mt_login.admin.list', [], [
      'query' => [
        'search' => $filters
      ]
    ]);
  }

  /**
   * Resets the filter selections.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $form_state->setRedirect('mt_login.admin.list');
  }

}
?>