<?php

namespace Drupal\mt_login\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\AliasStorageInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\mt_login\MTLoginProvider;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class MTLoginController.
 *
 * @package Drupal\mt_login\Controller
 */
class MTLoginController extends ControllerBase {

  public function list_login(Request $request) {

    $filters = $request->query->get('search');
    $build['mt_login_filters'] = $this->formBuilder()->getForm('Drupal\mt_login\Form\FiltersForm', $filters);
    $destination = $this->getDestinationArray();
    $class = new MTLoginProvider();

    $rows = [];
    $header = [
      0 => ['data' => $this->t('Id'), 'field' => 'id', 'sort' => 'desc'],
      1 => ['data' => $this->t('UID'), 'field' => 'uid'],
      2 => ['data' => $this->t('IP'), 'field' => 'ip'],
      3 => ['data' => $this->t('Type'), 'field' => 'type'],
    ];

    $data = $class->getTableLogin($header, 20, $filters);
    if (!empty($data)) {
      foreach ($data['table'] as $row) {
        $rows[] = [
          'data' => [
            'id' => $row->id,
            'uid' => $row->uid,
            'ip' => $row->ip,
            'type' => $row->type,
          ]
        ];
      }
    }

    $build['mt_login_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No records found.'),
    ];
    $build['mt_login_pager'] = array('#type' => 'pager');

    return $build;
  }

  public function api_login($params = [], $json = true) {
    if (empty($params)) $params = \Drupal::request()->query->all();
    $class = new MTLoginProvider();
    $register = $class->getAPILogin($params);
    if (!empty($register)) {
      $data['code'] = 200;
      $data['data'] = $register;
    } else {
      $data['code'] = 204;
    }
    if ($json) {
      return new JsonResponse($data);
    } else {
      return $data;
    }
  }

}
