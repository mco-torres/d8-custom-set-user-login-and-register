<?php

/**
 * @file
 * Contains \Drupal\mt_login/src/Controller/MTLoginProvider.php
 */

namespace Drupal\mt_login;

/**
 * Description of MTLoginProvider
 *
 * @author marco.torres
 */
class MTLoginProvider {

	/*
	* process data register custom Login
	*/
	public function processDataLogin($values) {
	    $output = [
	      'uid' => $values['login_uid'],
	      'ip' => $values['login_ip'],
	      'type' => $values['login_type'],
	    ];
	    return $output;
	}

	/*
	* get table registers custom Login
	*/
	public function getTableLogin($header, $num_per_page = 20, $filters = NULL) {
		try {
			$db = \Drupal\Core\Database\Database::getConnection();
		    $query = $db->select('mt_login','l');
			$query->fields('l',
				[
					'id',
					'uid',
					'ip',
					'type',
				]
			);

			if (!empty($filters)) {
				if (!empty($filters['id'])) $query->condition('id', $filters['id']);
				if (!empty($filters['uid'])) $query->condition('uid', $filters['uid']);
				if (!empty($filters['ip'])) $query->condition('ip', $filters['ip']);
				if (!empty($filters['type'])) $query->condition('type', $filters['type']);
			}

		    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
		                        ->orderByHeader($header);
		    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
		                        ->limit($num_per_page);
		    $result = $pager->execute();
    		$table = $result->fetchAll();
			if (!empty($table)) {
				return [
					'table' => $table,
					'pager' => $pager
				];
			}
			return FALSE;
		}
		catch (\Exception $e) {
      		return $e->getMessage();
		}

	}

	/*
	* get register custom Login with filters
	*/
	public function getAPILogin($params) {

        if (!empty($params['id'])) {
        	$id = is_array($params['id']) ? $params['id'] : explode(',', $params['id']);
        }
		if (empty($params['all'])) {
			$limit = !empty($params['limit']) ? $params['limit'] : 50;
			$offset = !empty($params['offset']) ? $params['offset'] : 0;
		}

		try {
    		$db = \Drupal\Core\Database\Database::getConnection();
			$query = $db->select('mt_login','l');
			$query->fields('l',
				[
					'id',
					'name',
					'status',
					'created',
					'modified',
				]
			);
			if (!empty($id)) $query->condition('l.id', $id, 'IN');
			if (!empty($params['uid'])) $query->condition('l.uid', $params['uid']);
			if (!empty($params['ip'])) $query->condition('l.ip', $params['ip']);
			if (!empty($params['type'])) $query->condition('l.type', $params['type']);
			if (empty($params['all'])) $query->range($offset, $limit);
			$output = $query->execute()->fetchAll();
			if (!empty($output)) return $output;
			return FALSE;
		}
		catch (Exception $e) {
			return FALSE;
		}
		return FALSE;
	}

	/*
	* create register custom Login
	*/
	public function createLogin($data) {
		try {
			$date_time = time();
    		$db = \Drupal\Core\Database\Database::getConnection();
			$query = $db->insert('mt_login');
			$query->fields(
				[
					'uid' => $data['uid'],
					'ip' => $data['ip'],
					'type' => $data['type'],
					'date' => $date_time,
				]
			);
			$query->execute();
			return TRUE;
		}
		catch (Exception $e) {
			return FALSE;
		}
	}
}

